#! /usr/bin/env python3
"""
Provide minimal switching behavior between joystick and autonomous control
via effort. This provides a way for us to patch in to the GSS stack and
yield control to GSS's OPENMNGR.

Joystick always immediately takes control, but only for commands effecting
vehicle motion. It should be possible to use the joystick to control
lights/tilt while still using the auto effort.

After joystick mode has been enabled, auto effort commands will not be forwarded
until an "enable auto effort" message is received.
"""


# When running via process_client on opensea hub, PYTHONPATH isn't set from
# ~/.bashrc, so set it here.
import sys
sys.path.append("/usr/local/share/gss_python_modules")
sys.path.append("/home/gss/.local/lib/python3.6/site-packages")  # for LCM
print("Using pythonpath: {}".format(sys.path))

import lcm

from gss import analog_t, pcomms_t

class EffortArbiter(object):
    def __init__(self):
        # Will change to "AUTO" when that's enabled, and switch back to JOYSTICK as soon as we get a non-zero joystick command.
        # QUESTION: DAQ_CMD is published continually; will OPENMNGR timeout if it's not received?
        self.mode = "JOYSTICK"

        self.lc = lcm.LCM()

        joystick_topic = "JOYSTICK_DAQ_CMD"
        self.joystick_sub = self.lc.subscribe(joystick_topic,
                                             self.handle_joystick)
        auto_topic = "AUTO_DAQ_CMD"
        self.auto_sub = self.lc.subscribe(auto_topic,
                                          self.handle_auto)

        enable_auto_topic = "ENABLE_AUTO_DAQ_CMD"
        self.enable_sub = self.lc.subscribe(enable_auto_topic,
                                            self.handle_enable)

        self.output_topic = "OPENMNGR_DAQ_CMD"

    def run(self):
        while True:
            self.lc.handle()

    def handle_joystick(self, _channel, data):
        msg = pcomms_t.decode(data)

        # Check if ANY of the values are non-zero
        for aa in msg.analogs:
            if aa.value != 0:
                if self.mode != "JOYSTICK":
                    print("{} has value {}; resuming JOYSTICK mode".format(aa.name, aa.value))
                self.mode = "JOYSTICK"
        if self.mode == "JOYSTICK":
            joy_msg = self.zero_other_axes(msg)
            self.lc.publish(self.output_topic, joy_msg.encode())

    def handle_auto(self, _channel, data):
        if self.mode == "AUTO":
            msg = pcomms_t.decode(data)
            joy_msg = self.zero_other_axes(msg)
            self.lc.publish(self.output_topic, joy_msg.encode())

    def handle_enable(self, _channel, _data):
        print("Enabling AUTO mode")
        self.mode = "AUTO"

    def zero_other_axes(self, input_msg):
        # Ensure that all unused axes are zeroed out.
        # (Our joystick doesn't always have fields for PHI and THETA,
        # which causes problems if an auto was using them, since they
        # are NOT zeroed out on subsequent messages)
        print("Zeroing axes in {} mode!".format(self.mode))
        joy_msg = pcomms_t()
        joy_cmd = {aa.name: aa.value for aa in input_msg.analogs}
        print(joy_cmd)
        for axis in ["X", "Y", "Z", "PSI", "THETA", "PHI"]:
            aa = analog_t()
            aa.name = "U_DAQ_JOY_" + axis
            if aa.name in joy_cmd:
                aa.value = joy_cmd[aa.name]
            else:
                aa.value = 0.0
            print(aa.name, aa.value)
            joy_msg.analogs.append(aa)
        joy_msg.num_analogs = len(joy_msg.analogs)
        return joy_msg



if __name__ == "__main__":
    ea = EffortArbiter()
    ea.run()
