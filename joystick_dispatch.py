#! /usr/bin/env python3
"""
Top-level mode switching between Greensea and ROS control chains.
* Press left trigger to enable Greensea
* Press right trigger to enable ROS

"""

# When running via process_client on opensea hub, PYTHONPATH isn't set from
# ~/.bashrc, so set it here.
import sys

sys.path.append("/usr/local/share/gss_python_modules")
sys.path.append("/home/gss/.local/lib/python3.6/site-packages")  # for LCM
print("Using pythonpath: {}".format(sys.path))
import lcm

from gss import daq_data_t  # Used for joystick messages
from gss import message_t  # Used for status messages


class JoystickArbiter(object):
    def __init__(self):
        self.lc = lcm.LCM()
        input_topic = "TOPSIDE_XBOX_STAT"

        self.ros_joy_topic = "ROS_XBOX_STAT"
        self.gss_joy_topic = "GSS_XBOX_STAT"

        self.mode_topic = "SYSTEM_MODE"  # Will be "ROS" or "GSS"
        self.mode = "GSS"  # Default to GSS

        self.lc.subscribe(input_topic, self.handle_joy)

    def handle_joy(self, _topic, data):
        # First, check if we need to switch modes!
        joy_msg = daq_data_t.decode(data)
        # the message's analog and digital arrays deserialize into maps
        # rather than lists/tuples, and then can't be reserialized.
        # So, need to convert to list!
        # (This operation consumes the map, so can only be done once!)
        joy_msg.digitals = list(joy_msg.digitals)
        joy_msg.analogs = list(joy_msg.analogs)

        if joy_msg.analogs[2] > 40000:
            if self.mode == "ROS":
                print("Switching to Greensea control with autos enabled!")
                self.mode = "GSS"
                # TODO: If we want to issue any other commands at this point, do them here.
        elif joy_msg.digitals[4]:
            if self.mode == "ROS":
                print("Switching to Greensea control, but drifting")
                self.mode = "GSS"
        elif joy_msg.analogs[5] > 50000:
            if self.mode == "GSS":
                print("Switching to ROS control")
                self.mode = "ROS"

        if self.mode == "GSS":
            self.lc.publish(self.gss_joy_topic, data)
        else:
            self.lc.publish(self.ros_joy_topic, data)
            # We also need to publish the message to the GSS stack, for control
            # of lights & tilt. However, zero out the stalks and dpad to prevent
            # accidentally updating depth/waypoint goals.
            analogs = list(joy_msg.analogs)
            analogs[0] = 32766  # left stalk, left-right
            analogs[1] = 32766  # left stalk, fwd-back
            analogs[3] = 32766  # right stalk, left-right
            analogs[4] = 32766  # right stalk, fwd-back
            analogs[6] = 32766  # dpad, left-right
            analogs[7] = 32766  # dpad, fwd-back
            joy_msg.analogs = analogs
            self.lc.publish(self.gss_joy_topic, joy_msg.encode())

        # May want to have this published on a callback as well,
        # in case the joystick is unplugged and we have no way to
        # tell the cmd_arbiter to go back to greensea mode.
        status_msg = message_t()
        status_msg.name = "SYSTEM_MODE"
        status_msg.value = self.mode
        self.lc.publish(self.mode_topic, status_msg.encode())

    def run(self):
        while True:
            self.lc.handle()


if __name__ == "__main__":
    ja = JoystickArbiter()
    ja.run()
