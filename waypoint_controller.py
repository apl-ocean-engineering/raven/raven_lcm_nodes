#! /usr/bin/env python3

"""
Waypoint control where each axis is handled independently.
Once a waypoint is "achieved", will hold station until new goal is sent.

Interfaces:
* (sub) OPENINS_NAV_SOLUTION: nav_solution_t -- curent vehicle position
* (sub) WAYPOINT_GOAL: pose_stat_t -- goal pose for next waypoint
* (pub) WAYPOINT_EFFORT: effort_t -- output effort along each axis
"""
# Have to set PYTHONPATH manually to run on opensea hub
import sys

sys.path.append("/usr/local/share/gss_python_modules")  # for GSS messages defs
sys.path.append("/home/gss/.local/lib/python3.6/site-packages")  # for LCM
print("Using pythonpath: {}".format(sys.path))

import lcm
from gss import analog_t, effort_t, nav_solution_t, pcomms_t, pose_stat_t

import math


class AxisController:
    """
    Single-axis PID controller
    """

    def __init__(self, p_gain, i_gain, d_gain, min_i, max_i, min_output, max_output):
        self.p_gain = p_gain
        self.i_gain = i_gain
        self.d_gain = d_gain
        self.prev_t = None
        self.curr_i = 0
        # Min/max of integrator windup
        self.min_i = min_i
        self.max_i = max_i
        # Min/max of output effort
        self.min_cmd = min_output
        self.max_cmd = max_output

        self.goal = None

    def calc_error(self, value):
        """
        For translational motions, error calculation is simple
        (Though the sign of this isn't what I'd expect, but I think it matches how GSS does it.
        k_p * error = effort, so if a value is too low, you need a positive error for signs to work out.)
        """
        error = self.goal - value
        return error

    def step(self, timestamp, value, velocity):
        """
        Step the PID controller forward with input state value and velocity.
        Returns commanded effort and error.
        """
        if self.goal is None:
            print("Don't have a goal!")
            return None, None

        # Can't calculate integral term for first state update since
        # we don't know what the time step is.
        if self.prev_t is None:
            dt = 0
        else:
            dt = timestamp - self.prev_t
        self.prev_t = timestamp

        error = self.calc_error(value)
        # For easy comparison with GSS's published intermediate values,
        # windup also includes gain.
        ii = self.curr_i + dt * self.i_gain * error
        self.curr_i = min(self.max_i, max(self.min_i, ii))

        # Minus sign for d term is because derivative of the error is inverted
        # from derivative of the state.
        pid = self.p_gain * error + self.curr_i - self.d_gain * velocity

        # print(
        #     "Goal {:0.1f}, current: {:0.1f}, error: {:0.1f}, sum_i: {:0.1f}, pid: {:0.1f}".format(
        #         self.goal, value, error, ii, pid
        #     )
        # )
        effort = max(self.min_cmd, min(self.max_cmd, pid))
        return effort, error

    def set_goal(self, goal):
        """
        New goals will be received infrequently.
        In a departure from GSS's system, reset the integral windup when
        a new goal is reached.
        """
        self.goal = goal
        self.curr_i = 0


class RotationController(AxisController):
    def calc_error(self, value):
        """
        For rotations, we need to handle angles wrapping around 0 -> 2pi.
        This controller makes no effort to unwind, simply making sure that the error is in [-pi, pi].
        """
        error = self.goal - value
        while error > math.pi:
            error -= 2 * math.pi
        while error < -1 * math.pi:
            error += 2 * math.pi
        return error


class TranslationController:
    def __init__(self, xgains, ygains):
        """
        For translations, we need to transform errors in northing, easting to
        errors in vehicle-frame before using them in the controller.
        """
        # I don't love this architecture.
        self.x_controller = AxisController(*xgains)
        self.y_controller = AxisController(*ygains)
        self.x_controller.set_goal(0)
        self.y_controller.set_goal(0)
        self.goal_northing = None
        self.goal_easting = None

    def set_goal(self, northing, easting):
        self.goal_northing = northing
        self.goal_easting = easting
        # Reset goal to zero out integral terms
        self.x_controller.set_goal(0)
        self.y_controller.set_goal(0)

    def step(self, timestamp, northing, easting, heading, vel_n, vel_e):
        # Convert from NED to vehicle-relative
        de = self.goal_easting - easting
        dn = self.goal_northing - northing
        xx = dn * math.cos(heading) + de * math.sin(heading)
        yy = de * math.cos(heading) - dn * math.sin(heading)
        # print("For dN, dE = {}, {}, (dx, dy) = {}, {}".format(dn, de, xx, yy))
        robot_x_vel = vel_e * math.sin(heading) + vel_n * math.cos(heading)
        robot_y_vel = vel_e * math.cos(heading) - vel_n * math.sin(heading)
        # TODO: This is confusing!
        # The AxisControllers calculate error as `goal - value`
        # However, we've already calculated error here, so need to negate it
        # in order for the signs to be correct.
        effort_x, error_x = self.x_controller.step(timestamp, -xx, robot_x_vel)
        effort_y, error_y = self.y_controller.step(timestamp, -yy, robot_y_vel)
        efforts = (effort_x, effort_y)
        errors = (error_x, error_y)
        return efforts, errors


class WaypointController:
    def __init__(self, disable_xy, disable_z, disable_heading):
        self.disable_xy = disable_xy
        self.disable_z = disable_z
        self.disable_heading = disable_heading
        # TODO: make so much stuff here into parameters!
        translation_gains = [50, 2, 35, -50, 50, -100, 100]
        self.xy_controller = TranslationController(translation_gains, translation_gains)
        self.z_controller = AxisController(200, 20, 200, -70, 70, -100, 100)
        self.heading_controller = RotationController(90, 0.01, 75, -50, 50, -100, 100)

        self.lcm = lcm.LCM()

        self.has_goal = False

        self.lcm.subscribe("OPENINS_NAV_SOLUTION", self.handle_state)
        self.lcm.subscribe("WAYPOINT_GOAL", self.handle_goal)

    def handle_state(self, _channel, data):
        state_msg = nav_solution_t.decode(data)

        effort_msg = effort_t()
        # QUESTION: Do we want relative or absolute position?
        timestamp = state_msg.unix_time
        # print(
        #     "Got vehicle state! time = {}, abs_pos = {}, rel_pos = {}".format(
        #         timestamp, state_msg.absolute_position, state_msg.relative_position
        #     )
        # )

        # Don't bother making individual controllers report they don't have goals
        if not self.has_goal:
            return

        # Absolute position is [lon, lat, depth], while relative is UTM (zone 10)
        # lon = state_msg.absolute_position[0]
        # lat = state_msg.absolute_position[1]
        # depth = state_msg.absolute_position[2]
        nn = state_msg.relative_position[0]
        ee = state_msg.relative_position[1]
        zz = state_msg.relative_position[2]
        heading = math.radians(state_msg.attitude[2])  # [roll, pitch, yaw]
        # attitude is [roll, pitch, yaw], in degrees
        heading = math.radians(state_msg.attitude[2])
        vn = state_msg.relative_position_dot[0]
        ve = state_msg.relative_position_dot[1]
        vz = state_msg.relative_position_dot[2]
        v_heading = math.radians(state_msg.attitude_dot[2])

        xy_efforts, xy_errors = self.xy_controller.step(
            timestamp, nn, ee, heading, vn, ve
        )
        if self.disable_xy:
            effort_msg.effort_x = 0
            effort_msg.effort_y = 0
        else:
            effort_msg.effort_x = xy_efforts[0]
            effort_msg.effort_y = xy_efforts[1]

        effort_z, error_z = self.z_controller.step(timestamp, zz, vz)
        if self.disable_z:
            effort_msg.effort_z = 0
        else:
            effort_msg.effort_z = effort_z

        effort_psi, error_psi = self.heading_controller.step(
            timestamp, heading, v_heading
        )
        if self.disable_heading:
            effort_msg.effort_psi = 0
        else:
            effort_msg.effort_psi = effort_psi

        effort_msg.effort_phi = 0
        effort_msg.effort_theta = 0

        status_msg = pcomms_t()
        status_fields = [
            "ERROR_X",
            "ERROR_Y",
            "ERROR_Z",
            "ERROR_PHI",
            "ERROR_THETA",
            "ERROR_PSI",
            "ROBOT_N",
            "ROBOT_E",
            "ROBOT_PSI",
            "GOAL_N",
            "GOAL_E",
            "GOAL_Z",
            "GOAL_PHI",
            "GOAL_THETA",
            "GOAL_PSI",
        ]
        status_values = [
            xy_errors[0],
            xy_errors[1],
            error_z,
            0,
            0,
            error_psi,
            nn,
            ee,
            heading,
            self.xy_controller.goal_northing,
            self.xy_controller.goal_easting,
            self.z_controller.goal,
            0,
            0,
            self.heading_controller.goal,
        ]
        for name, value in zip(status_fields, status_values):
            aa = analog_t()
            aa.name = name
            aa.value = value
            status_msg.analogs.append(aa)
        status_msg.num_analogs = len(status_msg.analogs)
        self.lcm.publish("WAYPOINT_CONTROLLER_STAT", pcomms_t.encode(status_msg))

        self.lcm.publish("WAYPOINT_EFFORT", effort_t.encode(effort_msg))
        # print(
        #     "Published effort: x, y, z, th = {}, {}, {}, {}".format(
        #         effort_msg.effort_x,
        #         effort_msg.effort_y,
        #         effort_msg.effort_z,
        #         effort_msg.effort_psi,
        #     )
        # )

    def handle_goal(self, _channel, data):
        # We can't use the waypoint message for this because it's missing
        # orientation information entirely.
        # pose_stat_t is an option: position_{x,y,z}, attitude_{heading, rolll, pitch}_deg

        msg = pose_stat_t.decode(data)

        # print(
        #    "New goal Waypoint! x,y,z = {}, {}, {}; heading = {}".format(
        #        msg.position_x, msg.position_y, msg.position_z, msg.attitude_heading_deg
        #    )
        # )
        self.has_goal = True

        self.xy_controller.set_goal(msg.position_x, msg.position_y)
        self.z_controller.set_goal(msg.position_z)
        # Controller itself computes errors in radians
        self.heading_controller.set_goal(math.radians(msg.attitude_heading_deg))

    def run(self):
        while True:
            timeout_ms = 100
            self.lcm.handle_timeout(timeout_ms)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--disable_xy",
        action="store_true",
        help="Disable control in XY plane for easier debugging",
    )
    parser.add_argument(
        "--disable_z",
        action="store_true",
        help="Disable vertical control for easier debugging",
    )
    parser.add_argument(
        "--disable_heading",
        action="store_true",
        help="Disable heading control for easier debugging",
    )
    args = parser.parse_args()

    waypoint_controller = WaypointController(
        args.disable_xy, args.disable_z, args.disable_heading
    )
    waypoint_controller.run()
