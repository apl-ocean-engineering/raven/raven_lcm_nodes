#! /usr/bin/env python3
"""
Provide  switching behavior between Greensea-derived thruster commands
and ROS-derived thruster commands.
via effort. This provides a way for us to patch in to the GSS stack and
yield control to GSS's OPENMNGR.

Joystick always immediately takes control, but only for commands effecting
vehicle motion. It should be possible to use the joystick to control
lights/tilt while still using the auto effort.

After joystick mode has been enabled, auto effort commands will not be forwarded
until an "enable auto effort" message is received.
"""

# When running via process_client on opensea hub, PYTHONPATH isn't set from
# ~/.bashrc, so set it here.
import sys
sys.path.append("/usr/local/share/gss_python_modules")
sys.path.append("/home/gss/.local/lib/python3.6/site-packages")  # for LCM
print("Using pythonpath: {}".format(sys.path))

import lcm

from gss import daq_data_t
from gss import message_t

class ThrusterCommandArbiter(object):
    def __init__(self):
        # Valid modes are:
        # * "GSS" (accepting commands from saab_falcon_pcs)
        # * "ROS" (accepting commands via gss_translator/thrust_allocation)
        self.mode = "GSS"

        self.lc = lcm.LCM()

        mode_topic = "SYSTEM_MODE"
        self.mode_sub = self.lc.subscribe(mode_topic, self.handle_mode)
        #joystick_topic = "TOPSIDE_XBOX_STAT"
        #self.joystick_sub = self.lc.subscribe(joystick_topic,
        #                                     self.handle_joystick)
        gss_topic = "GSS_THRUSTER_CMD"
        self.gss_cmd_sub = self.lc.subscribe(gss_topic,
                                            self.handle_gss_cmd)
        ros_topic = "ROS_THRUSTER_CMD"
        self.ros_cmd_sub = self.lc.subscribe(ros_topic,
                                            self.handle_ros_cmd)

        self.output_topic = "SAAB_THRUSTER_CMD"

    def run(self):
        while True:
            self.lc.handle()

    def handle_mode(self, _channel, data):
        msg = message_t.decode(data)
        if self.mode != msg.value:
            print("Switching to {} mode!".format(msg.value))
            self.mode = msg.value

    def handle_joystick(self, _channel, data):
        msg = daq_data_t.decode(data)

        # For some reason, msg.digitals[4] doesn't work, since msg.digitals is a 'map' type
        buttons = [int(val) for val in msg.digitals]

        # Left bumper
        if buttons[4]:
            self.mode = "ROS"
        else:
            self.mode = "GSS"

    def handle_gss_cmd(self, _channel, data):
        if self.mode == "GSS":
            self.lc.publish(self.output_topic, data)

    def handle_ros_cmd(self, _channel, data):
        if self.mode == "ROS":
            self.lc.publish(self.output_topic, data)

if __name__ == "__main__":
    tca = ThrusterCommandArbiter()
    tca.run()
