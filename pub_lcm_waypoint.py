#! /usr/bin/env python3
"""
Command-line script that publishes an LCM message with a pose goal.
The goal is specified relative to the vehicle's current position.
"""

import argparse
import lcm

from gss import nav_solution_t, pose_stat_t

import math

class WaypointPublisher:
    def __init__(self, dx, dy, dz, dth):
        """
        Publish waypoint offset from current vehicle position by deltas.
        Deltas are specified in vehicle-relative frame (fwd-stbd-down),
        not NED. (Only matters for dx & dy, since dz and dth are the same in
        both frames.)
        """
        self.dx = dx
        self.dy = dy
        self.dz = dz
        self.dth = dth
        self.goal = None
        self.lc = lcm.LCM()
        self.lc.subscribe("OPENINS_NAV_SOLUTION", self.handle_state)

    def handle_state(self, _channel, data):
        msg = nav_solution_t.decode(data)
        heading_deg = msg.attitude[2]

        print(
            "Got Nav solution! pos = {}, heading = {}".format(
                msg.relative_position, heading_deg
            )
        )
        heading = math.radians(heading_deg)
        dn = self.dx * math.cos(heading) - self.dy * math.sin(heading)
        de = self.dx * math.sin(heading) + self.dy * math.cos(heading)
        print("For dx = {} and dy = {} at heading {}, dn = {}, de = {}".format(self.dx, self.dy, heading_deg, dn, de))

        self.goal = pose_stat_t()
        self.goal.position_x = msg.relative_position[0] + dn
        self.goal.position_y = msg.relative_position[1] + de
        self.goal.position_z = msg.relative_position[2] + self.dz
        self.goal.position_valid = True

        goal_heading = heading_deg + self.dth
        while goal_heading > 360:
            goal_heading -= 360
        while goal_heading < 0:
            goal_heading += 360
        print(
            "Current heading: {:0.1f}, goal: {:0.1f}".format(heading_deg, goal_heading)
        )

        self.goal.attitude_heading_deg = goal_heading
        self.goal.attitude_pitch_deg = 0.0
        self.goal.attitude_roll_deg = 0.0
        self.goal.attitude_valid = True

    def run(self):
        while self.goal is None:
            self.lc.handle()
        print(
            "Publishing goal! position = {}, {}, {}; heading = {}".format(
                self.goal.position_x,
                self.goal.position_y,
                self.goal.position_z,
                self.goal.attitude_heading_deg,
            )
        )
        self.lc.publish("WAYPOINT_GOAL", self.goal.encode())


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dx", type=float, default=0.0, help="Desired change in X position (m)"
    )
    parser.add_argument(
        "--dy", type=float, default=0.0, help="Desired change in Y position (m)"
    )
    parser.add_argument(
        "--dz", type=float, default=0.0, help="Desired change in depth (m)"
    )
    parser.add_argument(
        "--dth", type=float, default=0.0, help="Desired change in heading (deg)"
    )
    args = parser.parse_args()

    wp = WaypointPublisher(args.dx, args.dy, args.dz, args.dth)
    wp.run()
