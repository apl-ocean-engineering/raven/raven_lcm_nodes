APL-written LCM nodes that are used in the GSS control pipeline.

* feedforward.py: hacky feedforward control for velocity mode

* effort_arbiter.py: switch between joystick-based DAQ input and programmatically generated DAQ signals.

Used to support autonomous effort control.
