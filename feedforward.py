#!/usr/bin/env python3


# When running via process_client on opensea hub, PYTHONPATH isn't set from
# ~/.bashrc, so set it here.
import sys
sys.path.append("/usr/local/share/gss_python_modules")
sys.path.append("/home/gss/.local/lib/python3.6/site-packages")  # for LCM
print("Using pythonpath: {}".format(sys.path))

import lcm

from gss import effort_t
from gss import cmd_t


class FeedForward(object):
    def __init__(self):
        self.lc = lcm.LCM()
        self.effort_sub = self.lc.subscribe("OPENCMD_EFFORT", self.handle_effort)
        self.cmd_sub = self.lc.subscribe("OPENCMD_CMD", self.handle_cmd)
        self.cmd = None  # Have to cache cmd since it's used by the effort callback
        self.cmd_timestamp = None  # don't use stale commands (if it's stale, will act as passthrough, regardless of the content of cmd)
        self.cmd_timeout = 0.1  # published at something well over 10Hz (45?)

        self.ff_effort_topic = "FF_EFFORT"


    def run(self):
        try:
            while True:
                self.lc.handle()
        except KeyboardInterrupt:
            pass

    def handle_cmd(self, channel, data):
        print("Got cmd message!")
        lcm_msg = cmd_t.decode(data)
        self.cmd = lcm_msg
        self.cmd_timestamp = lcm_msg.time_unix_sec


    def handle_effort(self, channel, data):
        print("Got effort message!")
        lcm_msg = effort_t.decode(data)

        if self.cmd is None or self.cmd_timestamp is None:
            print("Received EFFORT without cached CMD; passing it through")
            self.lc.publish(self.ff_effort_topic, data)  # or lcm_msg.encode()
            return

        dt = lcm_msg.time_unix_sec - self.cmd_timestamp
        if dt > self.cmd_timeout or dt < 0:
            print("WHOA, unexpected dt: {}, not calculating FF".format(dt))
            self.lc.publish(self.ff_effort_topic, data)  # or lcm_msg.encode()
            return

        if self.cmd.enable_state_x == 7:
            lcm_msg.effort_x += self.calc_x_ff(self.cmd.cmd_x)
        if self.cmd.enable_state_y == 7:
            lcm_msg.effort_y += self.calc_y_ff(self.cmd.cmd_y)  # In velocity mode, cmd_y will be in m/s
        self.lc.publish(self.ff_effort_topic, lcm_msg.encode())

    # TODO: actually provide proper lookup table, rather than this ridiculousness.
    def calc_y_ff(self, vel):
        """ Return percent effort required to maintain the input velocity along the x axis"""
        return 100*vel

    def calc_x_ff(self, vel):
        return 100*vel

if __name__ == "__main__":
    ff = FeedForward()
    ff.run()


