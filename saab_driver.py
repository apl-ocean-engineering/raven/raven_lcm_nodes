#! /usr/bin/env python3

"""
Simple node that replaces gss_saab_falcon, and provides an interface
to the thrusters and (temporarily) the tilt motor.
TODOs:
* Is it possible to combine "?R" with a command, thus saving a
  call-response cycle?
* Look at thruster responses to determine whether RPM is linear in
  percent effort. (use joystick + scaling to do this)

With 10 thrusters, we see ~10Hz update rates, which is sufficient.

To run on the command line:

./saab_driver.py '{"THRUSTER_1":"1", "THRUSTER_2":"2", "THRUSTER_7":"7", "THRUSTER_8":"8", "THRUSTER_9":"9", "THRUSTER_A":"A", "THRUSTER_B":"B"}' true

Interfaces:
* (sub) SAAB_THRUSTER_CMD: pcomms_t, "THRUSTER_#"
* (sub) SAAB_TILT_CMD: pcomms_t, "TILT"
* (pub) THRUSTER_DRIVER_STATS: pcomms_t, {"latency", "dt"}
* (pub) TILT_DRIVER_STATS: pcomms_t, {"latench", "dt"}
* (pub) THRUSTER_RPM: pcomms_t, "THRUSTER_#"  (propellor RPM; mult by 5 for motor RPM)
* (pub) SAAB_FALCON_STAT: pcomms_t, "TILT"   (matching fbk from GSS's driver)

We're using the (problematic) pcomms_t data type for output since it works
with the various greensea tools, and doesn't require managing our own custom
message definitions.

"""

# When running via process_client on opensea hub, PYTHONPATH isn't set from
# ~/.bashrc, so set it here.
import sys

sys.path.append("/usr/local/share/gss_python_modules")  # for GSS messages defs
sys.path.append("/home/gss/.local/lib/python3.6/site-packages")  # for LCM
print("Using pythonpath: {}".format(sys.path))

import argparse
import copy
import json
import lcm
import re
import serial
import threading
import time

from gss import analog_t, pcomms_t

# We haven't installed mypy on opensea
try:
    from typing import Dict
except:
    pass


class SaabDriver(object):
    def __init__(
        self,
        thrusters: Dict[str, str],  # Map from name in pcomms_t analog to SAAB address
        publish_rpm: bool,  # Whether to publish thruster RPM (may slow down control loop)
        disable_tilt: bool,  # Whether to disable tilt control (and don't try to query for feedback)
        thruster_timeout: float,  # Timeout (in seconds) after which we will send 0 commands to thrusters
    ):
        # Verify that input thruster ID is single-char string matching SAAB addr
        pattern = re.compile("[A-Z,0-9]")
        for key, val in thrusters.items():
            if pattern.fullmatch(val) is None:
                errmsg = "Invalid ID for thruster {}: {}".format(key, val)
                raise (Exception(errmsg))
        self.thruster_lookup = {
            key: bytes(val, "utf-8") for key, val in thrusters.items()
        }
        self.thruster_reverse_lookup = {
            val: key for key, val in self.thruster_lookup.items()
        }

        self.publish_rpm = publish_rpm
        self.disable_tilt = disable_tilt
        # Used to track whether incoming commands have stopped and we should
        # tell the thrusters to stop. (Tickling them for RPM seems to keep their
        # own timeouts from working as hoped.)
        self.command_timeout = thruster_timeout  # seconds
        self.last_command_time = 0

        print(
            "Initialized SaabDriver with pub_rpm = {} and thrusters = {}".format(
                self.publish_rpm, self.thruster_lookup
            )
        )

        self.ser = serial.Serial("/dev/ttyMAX1", 57600, timeout=0.05)
        self.lc = lcm.LCM()
        # I was lazy and hard-coded channels here and in cmd_demuxer.
        # Easy enough to change if we actually need flexibility.
        # NOTE(lindzey): LCM introduces a delay with the queue_capacity, since it
        #    queues the FIRST message, rather than having new message replace the
        #    not-yet-handled one. So, we're using our own thread + caching the most
        #    recent data, rather than using sub.set_queue_capacity(1)
        # NOTE(lindzey): The thruster commands come in at 50 Hz, but the tilt only
        #    comes in at 5 Hz. Given that the serial loop runs at ~10Hz, this means
        #    that the tilt commands will almost always have a ~200ms latency.
        #    This is fine because we're ditching the SAAB tilt ASAP.
        self.thruster_channel = "SAAB_THRUSTER_CMD"
        # Threading version
        self.thruster_sub = self.lc.subscribe(
            self.thruster_channel, self.thruster_callback
        )
        self.tilt_channel = "SAAB_TILT_CMD"
        if not self.disable_tilt:
            self.tilt_sub = self.lc.subscribe(self.tilt_channel, self.tilt_callback)

        # Simpler version
        # self.thruster_sub = self.lc.subscribe(self.thruster_channel, self.handle_thruster)
        # self.thruster_sub.set_queue_capacity(1)
        # self.tilt_sub = self.lc.subscribe(self.tilt_channel, self.handle_tilt)
        # self.tilt_sub.set_queue_capacity(1)

        self.thruster_data = None
        # Lock for self.thruster_data. This is probably overkill, but I wanted
        # to prevent consistently executing half of the thrusts from one command
        # and half from the next.
        self.thruster_data_lock = threading.Lock()
        self.tilt_data = None

        self.cmd_thread = threading.Thread(target=self.loop_commands)

    def loop_commands(self):
        while True:
            # Copy data so it doesn't change on us while sending commands to individual thrusters
            thruster_data = None
            with self.thruster_data_lock:
                thruster_data = copy.deepcopy(self.thruster_data)
                self.thruster_data = None

            dt = time.time() - self.last_command_time
            if thruster_data is not None:
                self.handle_thruster(self.thruster_channel, thruster_data)
            elif dt > self.command_timeout:
                thruster_cmds = {th: 0 for th in self.thruster_lookup.keys()}
                self.send_thruster_commands(thruster_cmds)

            # Publishing feedback should be independent of whether a command is sent.
            if self.publish_rpm:
                self.get_feedback()

            if self.tilt_data is not None:
                self.handle_tilt(self.tilt_channel, self.tilt_data)
            self.tilt_data = None
            # time.sleep(0.0)

    def thruster_callback(self, _channel, data):
        msg = pcomms_t.decode(data)
        t0 = time.time()
        # print("Got message {} sec after publication", t0 - msg.time_unix_sec)
        with self.thruster_data_lock:
            t1 = time.time()
            # print("...Handling message {} sec after publication", t1 - msg.time_unix_sec)
            self.thruster_data = data

    def handle_thruster(self, _channel, data):
        msg = pcomms_t.decode(data)
        self.last_command_time = msg.time_unix_sec
        t0 = time.time()
        latency = t0 - msg.time_unix_sec
        thruster_cmds = {}
        for aa in msg.analogs:
            if aa.name in self.thruster_lookup.keys():
                # Input may be float, but we need commands in [-100, 100]
                cmd = int(round(aa.value))
                thresholded_cmd = min(100, max(-100, cmd))
                thruster_cmds[aa.name] = thresholded_cmd

        # I'd originally wanted to check `if None in cmds.values():`, but
        # the GSS control chain will only know about 10 thrusters
        # (at least to start), while we actually have 11
        if len(thruster_cmds) > 0:
            self.send_thruster_commands(thruster_cmds)
            t1 = time.time()
            stats_msg = pcomms_t()
            latency_aa = analog_t()
            latency_aa.name = "latency"
            latency_aa.value = latency
            stats_msg.analogs.append(latency_aa)
            dt_aa = analog_t()
            dt_aa.name = "dt"
            dt_aa.value = t1 - t0
            stats_msg.analogs.append(dt_aa)
            stats_msg.num_analogs = len(stats_msg.analogs)
            stats_msg.time_unix_sec = time.time()
            self.lc.publish("THRUSTER_DRIVER_STATS", stats_msg.encode())
        else:
            pass
            # print("Warning -- received SAAB_FALCON_CMD, but names don't match")
            # print("cmd: {}, configured names: {}".format([aa.name for aa in msg.analogs], self.thruster_lookup))

    def tilt_callback(self, _channel, data):
        self.tilt_data = data

    def handle_tilt(self, _channel, data):
        msg = pcomms_t.decode(data)
        t0 = time.time()
        # Time between when message sent and when it's acted on
        latency = t0 - msg.time_unix_sec
        tilt_cmd = None
        for aa in msg.analogs:
            if aa.name == "TILT":
                cmd = int(round(aa.value))
                tilt_cmd = min(100, max(-100, cmd))

        # I'd originally wanted to check `if None in cmds.values():`, but
        # the GSS control chain will only know about 10 thrusters
        # (at least to start), while we actually have 11
        if tilt_cmd is not None:
            self.send_tilt_command(tilt_cmd)
            t1 = time.time()
            stats_msg = pcomms_t()
            latency_aa = analog_t()
            latency_aa.name = "latency"
            latency_aa.value = latency
            stats_msg.analogs.append(latency_aa)
            dt_aa = analog_t()
            dt_aa.name = "dt"
            dt_aa.value = t1 - t0
            stats_msg.analogs.append(dt_aa)
            stats_msg.num_analogs = len(stats_msg.analogs)
            stats_msg.time_unix_sec = time.time()
            self.lc.publish("TILT_DRIVER_STATS", stats_msg.encode())
        else:
            print("ERROR: SAAB_TILT_CMD doesn't have TILT field. msg = {}".format(msg))

    def send_thruster_commands(self, cmds):
        # type:(Dict[str, int]) -> None
        # GSS only ever commands the 10 thrusters, but the ROS message has 11.
        # This led to the 11th thruster running at a constant rpm after
        # switching back from ROS -> GSS mode. I think the problem was that since
        # we were still querying for feedback, the 1-sec timeout never triggered.
        for thruster_name, addr in self.thruster_lookup.items():
            try:
                val = cmds[thruster_name]
            except:
                val = 0
            if val >= 0:
                command = b"u%c8+%03d\r" % (addr, val)
            else:
                command = b"u%c8-%03d\r" % (addr, -1 * val)

            self.ser.write(command)

            response = self.ser.read_until(b"\r")
            if len(response) == 0:
                msg = "{:0.3f}: No response from thruster: {} -> {}".format(
                    time.time(), command, response
                )
                # print(msg)
                continue
            # TODO: Log all data sent to / received from the serial port.
            expected_response = b"U%c4\r" % (addr)
            if response != expected_response:
                print(
                    "{:0.3f}: For command {}, expected {} ({}) but got {} ({})".format(
                        time.time(),
                        command,
                        expected_response,
                        [int(bb) for bb in expected_response],
                        response,
                        [int(bb) for bb in response],
                    )
                )

    def send_tilt_command(self, cmd):
        # type: (int) -> None
        # Tilt address is always 1
        if cmd > 0:
            command = b"t18+%03d\r" % (cmd)
        else:
            command = b"t18-%03d\r" % (-1 * cmd)
        self.ser.write(command)

        response = self.ser.read_until(b"\r")
        if len(response) == 0:
            print(
                "{:0.3f}: No response from tilt: {} -> {}".format(
                    time.time(), command, response
                )
            )
            return

        try:
            str_response = response.decode("utf-8")
        except UnicodeDecodeError:
            print(
                "{:0.3f}: Couldn't decode tilt response: {} -> {} ({})".format(
                    time.time(), command, response, [int(bb) for bb in response]
                )
            )
            return

        pattern = re.compile("T1[4-9A-Z](-?[0-9]{1,2})\r")
        match = pattern.fullmatch(str_response)
        if match is None:
            print(
                "{:0.3f}: Could not parse Tilt's angle: {} -> {} ({})".format(
                    time.time(), command, response, [int(bb) for bb in response]
                )
            )
        else:
            tilt_deg = int(match.group(1))

            tilt_msg = pcomms_t()
            tilt_msg.time_unix_sec = time.time()
            aa = analog_t()
            aa.name = "TILT"
            aa.value = tilt_deg
            tilt_msg.analogs.append(aa)
            tilt_msg.num_analogs = len(tilt_msg.analogs)
            self.lc.publish("SAAB_FALCON_STAT", tilt_msg.encode())

    def get_feedback(self):
        addresses = [val for val in self.thruster_lookup.values()]
        addresses.sort()
        rpms = {}
        pattern = re.compile("U[0-9A-Z][4-9A-Z]R(-?[0-9]{1,4})\r")
        for addr in addresses:
            command = b"u%c6?R\r" % (addr)
            self.ser.write(command)
            response = self.ser.read_until(b"\r")
            if len(response) == 0:
                msg = "{:0.3f}: No response from thruster: {} -> {}".format(
                    time.time(), command, response
                )
                # print(msg)
                continue
            try:
                str_response = response.decode("utf-8")
            except UnicodeDecodeError:
                print(
                    "{:0.3f}: Couldn't decode thruster response: {} -> {} ({})".format(
                        time.time(), command, response, [int(bb) for bb in response]
                    )
                )
                continue
            match = pattern.fullmatch(str_response)
            if match is None:
                print(
                    "{:0.3f}: Could not parse thruster's RPM: {} -> {} ({})".format(
                        time.time(), command, response, [int(bb) for bb in response]
                    )
                )
            else:
                rpm = int(match.group(1))
                rpms[addr] = rpm
        rpm_msg = pcomms_t()
        rpm_msg.time_unix_sec = time.time()
        for addr, rpm in rpms.items():
            thruster = self.thruster_reverse_lookup[addr]
            aa = analog_t()
            aa.name = thruster
            aa.value = rpm / 5.0  # Thruster reports motor RPM, we care about Propeller.
            rpm_msg.analogs.append(aa)
        rpm_msg.num_analogs = len(rpm_msg.analogs)
        self.lc.publish("THRUSTER_RPM", rpm_msg.encode())

    def run(self):
        # TODO: Commands come in at 50Hz, and we need to discard any that we
        #       can't keep up with, rather than falling farther and farther behind.
        #       This is harder than it needs to be, because:
        #       1) LCM doesn't seem to have the equivalent of setting queue_size=1
        #          => WRONG: use sub.set_queue_capacity(1) (but the FIRST message received will win, rather than the last one)
        #       2) There are two types of messages being sent on the same channel,
        #          so we can't simply have a queue.
        #          => Redirect the tilt commands to a different topic. This was poor design on Greensea's part.
        #       The other option is to have two variables storing tilt/thruster commands, which are updated in
        # the callback thread. Then, in the main thread, take turns with tilt/thruster commands.
        self.cmd_thread.start()
        while True:
            timeout_ms = 100
            self.lc.handle_timeout(timeout_ms)
            # self.lc.handle()


if __name__ == "__main__":
    # Greensea uses a mix of command-line arguments and yaml files to configure
    # their LCM nodes.
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "thrusters",
        type=json.loads,
        help="Dict mapping thruster name (as used by Greensea) to address (as used by SAAB's protocol)",
    )
    parser.add_argument(
        "--publish_rpm",
        action="store_true",
        help="Whether to publish the thruster's reported RPM",
    )
    parser.add_argument(
        "--disable_tilt",
        action="store_true",
        help="Whether to disable tilt control",
    )
    parser.add_argument(
        "--command_timeout",
        type=float,
        default=1.0,
        help="Timeout (in seconds) on input commands after which thrusters will be set to 0.",
    )
    args = parser.parse_args()
    sd = SaabDriver(
        args.thrusters, args.publish_rpm, args.disable_tilt, args.command_timeout
    )
    sd.run()
