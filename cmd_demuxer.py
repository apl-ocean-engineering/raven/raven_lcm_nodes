#! /usr/bin/env python3
"""
Script that Demuxes Greensea's SAAB_FALCON_CMD into individual
light/thruster/tilt commands so we can use LCM's queue sizes
on commands for separate actuator types.

I haven't (yet?) gone as far as defining more strongly typed messages
for the individual commands; for now, just sticking with pcomms_t for
each.
"""

# When running via process_client on opensea hub, PYTHONPATH isn't set from
# ~/.bashrc, so set it here.
import sys
sys.path.append("/usr/local/share/gss_python_modules")
sys.path.append("/home/gss/.local/lib/python3.6/site-packages")  # for LCM
print("Using pythonpath: {}".format(sys.path))

import lcm

from gss import pcomms_t

class CommandDemuxer(object):
    def __init__(self):
        self.command_channel = "SAAB_FALCON_CMD"
        self.thruster_channel = "GSS_THRUSTER_CMD"
        self.light_channel = "SAAB_LIGHT_CMD"
        self.tilt_channel = "SAAB_TILT_CMD"

        self.lc = lcm.LCM()
        self.cmd_sub = self.lc.subscribe(self.command_channel, self.cmd_callback)

    def cmd_callback(self, _channel, data):
        msg = pcomms_t.decode(data)
        is_thruster_command = False
        is_tilt_command = False
        is_light_command = False
        for aa in msg.analogs:
            if "TILT" in aa.name:
                is_tilt_command = True
            elif "LIGHT" in aa.name:
                is_light_command = True
            elif "THRUSTER" in aa.name:
                is_thruster_command = True

        if is_tilt_command:
            if is_light_command or is_thruster_command:
                print("ERROR: message has multiple types of commands! {}".format(msg))
            self.lc.publish(self.tilt_channel, data)

        if is_light_command:
            if is_tilt_command or is_thruster_command:
                print("ERROR: message has multiple types of commands! {}".format(msg))
            self.lc.publish(self.light_channel, data)

        if is_thruster_command:
            if is_tilt_command or is_light_command:
                print("ERROR: message has multiple types of commands! {}".format(msg))
            self.lc.publish(self.thruster_channel, data)

    def run(self):
        while True:
            self.lc.handle()

if __name__ == "__main__":
    demuxer = CommandDemuxer()
    demuxer.run()

