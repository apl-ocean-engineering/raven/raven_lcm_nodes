#! /usr/bin/env python3
"""
Duplicate control allocation found within saab_falcon_pcs, for use
with the control chain accepting input from ROS waypoints.
"""

# When running via process_client on opensea hub, PYTHONPATH isn't set from
# ~/.bashrc, so set it here.
import sys

sys.path.append("/usr/local/share/gss_python_modules")
sys.path.append("/home/gss/.local/lib/python3.6/site-packages")  # for LCM
print("Using pythonpath: {}".format(sys.path))

import lcm
import time

from gss import analog_t, effort_t, pcomms_t


class ThrustAllocator(object):
    def __init__(self):
        self.lc = lcm.LCM()
        self.count_publish = 0

        # TODO: Mixing configuration with code is ugly =)

        # Range of input commands to thruster that map to ~linear part of
        # thruster command  <-> resulting RPM curve
        # [pos min, pos max, neg min, neg max]
        self.propvar = [
            [13, 91, 13, 90],  # 1
            [11, 87, 11, 86],  # 2
            [0, 100, 0, 100],  # Unused
            [0, 100, 0, 100],  # Unused
            [0, 100, 0, 100],  # Unused
            [0, 100, 0, 100],  # Unused
            [15, 92, 15, 93],  # 7
            [13, 93, 13, 91],  # 8
            [11, 91, 11, 92],  # 9 out stbd rear
            [14, 91, 14, 91],  # 10A  out port rear
            [15, 88, 15, 88],  # 11/B  vert rear
        ]

        # Whether thruster is installed s.t. positive commands yield positive thrust in vehicle frame
        # (Every motor has invert:true in propvars
        self.signs = 11 * [-1]

        # Control allocation matrix.
        # [x, y, z, roll, pitch, yaw]
        self.allocations = [
            [0, 0, 1, 1, 0, 0],  # TH 1; stbd vert
            [0, 0, 1, -1, 0, 0],  # TH2; port vert
            [1, -1, 0, 0, 0, -1],  # TH3; in stbd fwd
            [1, 1, 0, 0, 0, 1],  # TH4; in port fwd
            [1, 1, 0, 0, 0, -1],  # TH5; in stbd aft
            [1, -1, 0, 0, 0, 1],  # TH6; in port aft
            [1, -1, 0, 0, 0, -1],  # TH7; out stbd fwd
            [1, 1, 0, 0, 0, 1],  # TH8; out port fwd
            [1, 1, 0, 0, 0, -1],  # TH9; out stbd aft
            [1, -1, 0, 0, 0, 1],  # THA; out port aft
            [0, 0, 0, 0, 1, 0],  # THB; aft vert
        ]

        self.thruster_names = [
            "THRUSTER_1",
            "THRUSTER_2",
            "THRUSTER_3",
            "THRUSTER_4",
            "THRUSTER_5",
            "THRUSTER_6",
            "THRUSTER_7",
            "THRUSTER_8",
            "THRUSTER_9",
            "THRUSTER_A",
            "THRUSTER_B",
        ]

        # Topic to publish output thruster commands to
        self.thruster_topic = "ROS_THRUSTER_CMD"
        # Subscribe to incoming effort commands
        effort_topic = "WAYPOINT_EFFORT"
        self.effort_sub = self.lc.subscribe(effort_topic, self.handle_effort)

    def run(self):
        while True:
            self.lc.handle()

    def handle_effort(self, _channel, data):
        effort_msg = effort_t.decode(data)
        effort = [
            effort_msg.effort_x,
            effort_msg.effort_y,
            effort_msg.effort_z,
            effort_msg.effort_phi,
            effort_msg.effort_theta,
            effort_msg.effort_psi,
        ]

        # opensea doesn't have enough space to install numpy.
        # So, until we switch everything over to run on the SD card,
        # doing this by hand.
        # thrusts = np.dot(self.allocations, effort)
        thrusts = [
            sum(
                [
                    self.allocations[ii][jj] * effort[jj]
                    for jj in range(len(self.allocations[ii]))
                ]
            )
            for ii in range(len(self.allocations))
        ]

        for axis, thrust in enumerate(thrusts):
            if thrust >= 0:
                delta = self.propvar[axis][1] - self.propvar[axis][0]
                thrust = self.propvar[axis][0] + thrust * delta / 100
                thrust = min(self.propvar[axis][1], thrust)
            else:
                delta = self.propvar[axis][3] - self.propvar[axis][2]
                thrust = -1 * self.propvar[axis][2] + thrust * delta / 100
                thrust = max(-1 * self.propvar[axis][3], thrust)

            thrust = int(round(thrust))
            thrust *= self.signs[axis]
            thrusts[axis] = thrust

        thruster_msg = pcomms_t()
        thruster_msg.time_unix_sec = time.time()
        thruster_msg.count_publish = self.count_publish
        for axis, thruster in enumerate(self.thruster_names):
            aa = analog_t()
            aa.name = thruster
            aa.value = thrusts[axis]
            thruster_msg.analogs.append(aa)
        thruster_msg.num_analogs = len(thruster_msg.analogs)

        # print("Output commands: {}".format(thrusts))
        # print("Publishing {} analogs", len(thruster_msg.analogs))
        self.lc.publish(self.thruster_topic, thruster_msg.encode())
        self.count_publish += 1


if __name__ == "__main__":
    ta = ThrustAllocator()
    ta.run()
