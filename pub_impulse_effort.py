#! /usr/bin/env python3

import lcm
import math
import sys
import time

from gss import effort_t


class ImpulseGenerator(object):
    def __init__(self, axis, max_val, delay, width):
        self.axis = axis
        self.max_val = max_val
        self.delay = delay
        self.width = width
        # For now, just take hte place of waypoint_control
        self.effort_topic = "WAYPOINT_EFFORT"
        self.sleep_dt = 0.1  # Run at 10 Hz
        self.t0 = time.time()
        self.lc = lcm.LCM()

    # TODO: Make this actually repeat the impulses, rather than just one-shot.
    def run(self):
        while True:
            effort_msg = effort_t()

            tt = time.time() - self.t0
            cmd = 0.0
            if tt > self.delay and tt < self.delay + self.width:
                cmd = self.max_val

            for axis in ["X", "Y", "Z", "PSI", "THETA", "PHI"]:
                field = "effort_" + axis.lower()
                if axis == self.axis:
                    setattr(effort_msg, field, cmd)
                else:
                    setattr(effort_msg, field, 0.0)
            self.lc.publish(self.effort_topic, effort_msg.encode())
            # QUESTION: Does LCM provide a better way to publish at a fixed rate?
            time.sleep(self.sleep_dt)

            if tt > 2 * self.delay + self.width:
                break


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "axis", type=str, help="Which axis to control. {X, Y, Z, PHI, PSI}"
    )
    parser.add_argument("max_val", type=float, help="maximum value")
    parser.add_argument("delay", type=float, help="delay before impulse")
    parser.add_argument("width", type=float, help="width of impulse")
    args = parser.parse_args()

    if args.axis not in ["X", "Y", "Z", "PSI", "THETA", "PHI"]:
        print("ERROR: invalid input axis ({})".format(args.axis))
        sys.exit()
    sg = ImpulseGenerator(args.axis, args.max_val, args.delay, args.width)
    sg.run()
